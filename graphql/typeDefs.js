const gql = require("graphql-tag");

const typeDefs = gql`
  scalar MongooseObjectId
  type Like {
    username: String!
    createdAt: String!
  }
  type Comment {
    username: String!
    body: String!
    createdAt: String!
    id: ID!
  }
  type Post {
    id: ID!
    body: String!
    createdAt: String!
    username: String!
    commentCount: Int!
    likeCount: Int!
    comments: [Comment]!
    likes: [Like]!
  }
  type User {
    id: ID!
    email: String!
    token: String!
    username: String!
    createdAt: String!
  }
  input RegisterInput {
    username: String!
    password: String!
    confirmPassword: String!
    email: String!
  }
  type Query {
    getPosts: [Post]
    getPost(postId: ID!): Post
  }
  type Mutation {
    register(registerInput: RegisterInput): User!
    login(username: String!, password: String!): User!
    createPost(body: String!): Post!
    deletePost(postId: ID!): String!
    toggleLikeOnPost(postId: ID!): Post!
    commentOnPost(postId: ID!, body: String!): Post!
    deletetCommentOnPost(postId: ID!, commentId: ID!): Post!
  }
`;

module.exports = typeDefs;
