const Post = require("../../models/Post");
const checkAuth = require("../../util/check-auth");
const {
  AuthenticationError,
  UserInputError,
  ForbiddenError,
} = require("apollo-server");
const { v4: uuid } = require("uuid");

module.exports = {
  Query: {
    async getPosts() {
      try {
        const posts = await Post.find().sort({ createdAt: "desc" });
        return posts;
      } catch (err) {
        throw new Error(err);
      }
    },
    async getPost(_, { postId }) {
      try {
        const post = await Post.findById(postId);
        if (post) {
          return post;
        } else {
          throw new Error("Post not found");
        }
      } catch (err) {
        throw new Error(err);
      }
    },
  },
  Mutations: {
    async createPost(_, { body }, context) {
      if (body.trim() == "") {
        throw new Error("Post must not be empty");
      }
      const user = checkAuth(context);
      const newPost = new Post({
        body,
        user: user._id,
        username: user.username,
        likeCount: 0,
        commentCount: 0,
        createdAt: new Date().toISOString(),
        comments: [],
        likes: [],
      });
      const post = await newPost.save();
      return post;
    },
    async deletePost(_, { postId }, context) {
      const user = checkAuth(context);
      try {
        const post = await Post.findById(postId);
        if (user.username === post.username) {
          await post.delete();
          return "Post Deleted Successfully";
        } else {
          throw new AuthenticationError("Action not Allowed");
        }
      } catch (err) {
        throw new Error(err);
      }
    },
    async toggleLikeOnPost(_, { postId }, context) {
      const user = checkAuth(context);
      let post = await Post.findById(postId);
      if (post) {
        let likeFound = false;
        for (i = 0; i < post.likes.length; i++) {
          if ((user.username = post.likes[i].username)) {
            likeFound = true;
            post.likes.splice(i, 1);
            post.likeCount--;
            break;
          }
        }
        if (!likeFound) {
          post.likes.push({
            username: user.username,
            createdAt: new Date().toISOString(),
          });
          post.likeCount++;
        }
        await post.save();
        return post;
      } else {
        throw new UserInputError("Post not found!");
      }
    },
    async commentOnPost(_, { postId, body }, context) {
      const user = checkAuth(context);
      const post = await Post.findById(postId);
      if (body.trim() === "") {
        throw new UserInputError("Comment body empty", {
          errors: { body: "Comment body must not be empty" },
        });
      }
      if (post) {
        post.comments.push({
          body: body,
          createdAt: new Date().toISOString(),
          username: user.username,
          id: uuid(),
        });
        post.commentCount++;
        await post.save();
        return post;
      } else {
        throw new UserInputError("Post not found!");
      }
    },
    async deletetCommentOnPost(_, { postId, commentId }, context) {
      const user = checkAuth(context);
      const post = await Post.findById(postId);
      if (post) {
        let commentFound = false;
        let tempComment;
        for (let i = 0; i < post.comments.length; i++) {
          commentFound = true;
          tempComment = post.comments[i];
          if (tempComment.id === commentId) {
            if (tempComment.username === user.username) {
              post.comments.splice(i, 1);
              post.commentCount--;
            } else {
              throw new ForbiddenError("User not authorized to delete comment");
            }
            break;
          }
        }
        if (!commentFound) {
          throw new UserInputError("Invalid Comment id provided");
        }
        await post.save();
        return post;
      } else {
        throw new UserInputError("Post not found!");
      }
    },
  },
};
